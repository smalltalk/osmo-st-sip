"
 (C) 2011-2014 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

PP.PPCompositeParserTest subclass: SIPQuotedStringParserTest [
    <category: 'OsmoSIP-Grammar'>

    parserClass [
        ^SIPQuotedStringParser
    ]

    testParseStrings [
        | res beenHere stream |

        beenHere := false.
        stream := '' readStream asPetitStream.
        self assert: stream position equals: 0.
        res := self parserInstance parse: stream onError: [beenHere := true].
        self assert: stream position equals: 0.
        self assert: beenHere.

        beenHere := false.
        stream := '"' readStream asPetitStream.
        self assert: stream position equals: 0.
        res := self parserInstance parse: stream onError: [beenHere := true].
        self assert: stream position equals: 0.
        self assert: beenHere.

        stream := '""' readStream asPetitStream.
        res := self parse: stream.
        self assert: stream atEnd.
        self assert: res equals: ''.


        stream := ' ""' readStream asPetitStream.
        res := self parse: stream.
        self assert: stream atEnd.
        self assert: res equals: ''.

        stream := '"abcdef0123-!"' readStream asPetitStream.
        res := self parserInstance parse: stream.
        self assert: stream atEnd.
        self assert: res equals: 'abcdef0123-!'.

        stream := '"\""' readStream asPetitStream.
        res := self parserInstance parse: stream.
        self assert: stream atEnd.
        self assert: res equals: '\"'.
    ]
]
