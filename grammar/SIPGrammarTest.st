"
 (C) 2011-2014 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

PP.PPCompositeParserTest subclass: SIPGrammarTest [
    <category: 'OsmoSIP-Grammar-Tests'>
    <comment: 'I try to parse some SIP messages.'>

    SIPGrammarTest class >> packageNamesUnderTest [
        <category: 'accessing'>
        ^ #('SIPGrammar')
    ]

    parserClass [
        <category: 'accessing'>
        ^ SIPGrammar
    ] 


    testTrying [
        | data res hdr |
        data := #(16r53 16r49 16r50 16r2F 16r32 16r2E 16r30 16r20
                  16r31 16r30 16r30 16r20 16r54 16r72 16r79 16r69
                  16r6E 16r67 16r0D 16r0A 16r56 16r69 16r61 16r3A
                  16r20 16r53 16r49 16r50 16r2F 16r32 16r2E 16r30
                  16r2F 16r55 16r44 16r50 16r20 16r31 16r37 16r32
                  16r2E 16r31 16r36 16r2E 16r32 16r35 16r34 16r2E
                  16r32 16r34 16r3B 16r72 16r70 16r6F 16r72 16r74
                  16r3D 16r35 16r30 16r36 16r30 16r3B 16r62 16r72
                  16r61 16r6E 16r63 16r68 16r3D 16r7A 16r39 16r68
                  16r47 16r34 16r62 16r4B 16r66 16r77 16r6A 16r6C
                  16r78 16r64 16r72 16r76 16r0D 16r0A 16r46 16r72
                  16r6F 16r6D 16r3A 16r20 16r22 16r7A 16r65 16r63
                  16r6B 16r65 16r22 16r20 16r3C 16r73 16r69 16r70
                  16r3A 16r31 16r30 16r30 16r30 16r40 16r6F 16r6E
                  16r2D 16r77 16r61 16r76 16r65 16r73 16r2E 16r63
                  16r6F 16r6D 16r3E 16r3B 16r74 16r61 16r67 16r3D
                  16r77 16r6D 16r79 16r63 16r6C 16r0D 16r0A 16r54
                  16r6F 16r3A 16r20 16r3C 16r73 16r69 16r70 16r3A
                  16r39 16r31 16r39 16r38 16r40 16r31 16r37 16r32
                  16r2E 16r31 16r36 16r2E 16r31 16r2E 16r37 16r32
                  16r3E 16r0D 16r0A 16r43 16r61 16r6C 16r6C 16r2D
                  16r49 16r44 16r3A 16r20 16r6F 16r66 16r63 16r77
                  16r6E 16r70 16r6D 16r75 16r6C 16r6D 16r63 16r65
                  16r61 16r73 16r67 16r40 16r78 16r69 16r61 16r6F
                  16r79 16r75 16r0D 16r0A 16r43 16r53 16r65 16r71
                  16r3A 16r20 16r39 16r38 16r33 16r20 16r49 16r4E
                  16r56 16r49 16r54 16r45 16r0D 16r0A 16r55 16r73
                  16r65 16r72 16r2D 16r41 16r67 16r65 16r6E 16r74
                  16r3A 16r20 16r46 16r72 16r65 16r65 16r53 16r57
                  16r49 16r54 16r43 16r48 16r2D 16r6D 16r6F 16r64
                  16r5F 16r73 16r6F 16r66 16r69 16r61 16r2F 16r31
                  16r2E 16r30 16r2E 16r68 16r65 16r61 16r64 16r2D
                  16r67 16r69 16r74 16r2D 16r64 16r66 16r66 16r34
                  16r31 16r61 16r66 16r20 16r32 16r30 16r31 16r31
                  16r2D 16r30 16r34 16r2D 16r32 16r30 16r20 16r31
                  16r34 16r2D 16r31 16r31 16r2D 16r32 16r34 16r20
                  16r2B 16r30 16r32 16r30 16r30 16r0D 16r0A 16r43
                  16r6F 16r6E 16r74 16r65 16r6E 16r74 16r2D 16r4C
                  16r65 16r6E 16r67 16r74 16r68 16r3A 16r20 16r30
                  16r0D 16r0A 16r0D 16r0A) asByteArray.

            res := self parse: data asString.
            self assert: res size equals: 4.
            self assert: res first third equals: '100'.
            self assert: (res first at: 5) equals: 'Trying'.

            hdr := res second.

            self assert: (hdr at: 1) first first equals: 'Via'.
            self assert: (hdr at: 1) first third asFoldedString equals: 'SIP/2.0/UDP 172.16.254.24;rport=5060;branch=z9hG4bKfwjlxdrv'.

            self assert: (hdr at: 2) first first equals: 'From'.
            self assert: (hdr at: 2) first third asFoldedString equals: '"zecke" <sip:1000@on-waves.com>;tag=wmycl'.

            self assert: (hdr at: 3) first first equals: 'To'.
            self assert: (hdr at: 3) first third asFoldedString equals: '<sip:9198@172.16.1.72>'.

            self assert: (hdr at: 4) first first equals: 'Call-ID'.
            self assert: (hdr at: 4) first third equals: 'ofcwnpmulmceasg@xiaoyu'.

            self assert: (hdr at: 5) first first equals: 'CSeq'.
            self assert: (hdr at: 5) first third asFoldedString equals: '983 INVITE'.

            self assert: (hdr at: 6) first first equals: 'User-Agent'.
            self assert: (hdr at: 6) first third equals: 'FreeSWITCH-mod_sofia/1.0.head-git-dff41af 2011-04-20 14-11-24 +0200'.

            self assert: (hdr at: 7) first first equals: 'Content-Length'.
            self assert: (hdr at: 7) first third equals: '0'.
    ]

    testInviteOK [
        | data res |
        data := #(16r53 16r49 16r50 16r2F 16r32 16r2E 16r30 16r20 16r32 16r30 16r30 16r20 16r4F 16r4B 16r0D 16r0A
                  16r56 16r69 16r61 16r3A 16r20 16r53 16r49 16r50 16r2F 16r32 16r2E 16r30 16r2F 16r55 16r44 16r50
                  16r20 16r31 16r37 16r32 16r2E 16r31 16r36 16r2E 16r32 16r35 16r34 16r2E 16r32 16r34 16r3B 16r72
                  16r70 16r6F 16r72 16r74 16r3D 16r35 16r30 16r36 16r30 16r3B 16r62 16r72 16r61 16r6E 16r63 16r68
                  16r3D 16r7A 16r39 16r68 16r47 16r34 16r62 16r4B 16r66 16r77 16r6A 16r6C 16r78 16r64 16r72 16r76
                  16r0D 16r0A 16r46 16r72 16r6F 16r6D 16r3A 16r20 16r22 16r7A 16r65 16r63 16r6B 16r65 16r22 16r20
                  16r3C 16r73 16r69 16r70 16r3A 16r31 16r30 16r30 16r30 16r40 16r6F 16r6E 16r2D 16r77 16r61 16r76
                  16r65 16r73 16r2E 16r63 16r6F 16r6D 16r3E 16r3B 16r74 16r61 16r67 16r3D 16r77 16r6D 16r79 16r63
                  16r6C 16r0D 16r0A 16r54 16r6F 16r3A 16r20 16r3C 16r73 16r69 16r70 16r3A 16r39 16r31 16r39 16r38
                  16r40 16r31 16r37 16r32 16r2E 16r31 16r36 16r2E 16r31 16r2E 16r37 16r32 16r3E 16r3B 16r74 16r61
                  16r67 16r3D 16r61 16r74 16r30 16r39 16r74 16r63 16r79 16r38 16r31 16r44 16r44 16r58 16r46 16r0D
                  16r0A 16r43 16r61 16r6C 16r6C 16r2D 16r49 16r44 16r3A 16r20 16r6F 16r66 16r63 16r77 16r6E 16r70
                  16r6D 16r75 16r6C 16r6D 16r63 16r65 16r61 16r73 16r67 16r40 16r78 16r69 16r61 16r6F 16r79 16r75
                  16r0D 16r0A 16r43 16r53 16r65 16r71 16r3A 16r20 16r39 16r38 16r33 16r20 16r49 16r4E 16r56 16r49
                  16r54 16r45 16r0D 16r0A 16r43 16r6F 16r6E 16r74 16r61 16r63 16r74 16r3A 16r20 16r3C 16r73 16r69
                  16r70 16r3A 16r39 16r31 16r39 16r38 16r40 16r31 16r37 16r32 16r2E 16r31 16r36 16r2E 16r31 16r2E
                  16r37 16r32 16r3A 16r35 16r30 16r36 16r30 16r3B 16r74 16r72 16r61 16r6E 16r73 16r70 16r6F 16r72
                  16r74 16r3D 16r75 16r64 16r70 16r3E 16r0D 16r0A 16r55 16r73 16r65 16r72 16r2D 16r41 16r67 16r65
                  16r6E 16r74 16r3A 16r20 16r46 16r72 16r65 16r65 16r53 16r57 16r49 16r54 16r43 16r48 16r2D 16r6D
                  16r6F 16r64 16r5F 16r73 16r6F 16r66 16r69 16r61 16r2F 16r31 16r2E 16r30 16r2E 16r68 16r65 16r61
                  16r64 16r2D 16r67 16r69 16r74 16r2D 16r64 16r66 16r66 16r34 16r31 16r61 16r66 16r20 16r32 16r30
                  16r31 16r31 16r2D 16r30 16r34 16r2D 16r32 16r30 16r20 16r31 16r34 16r2D 16r31 16r31 16r2D 16r32
                  16r34 16r20 16r2B 16r30 16r32 16r30 16r30 16r0D 16r0A 16r41 16r63 16r63 16r65 16r70 16r74 16r3A
                  16r20 16r61 16r70 16r70 16r6C 16r69 16r63 16r61 16r74 16r69 16r6F 16r6E 16r2F 16r73 16r64 16r70
                  16r0D 16r0A 16r41 16r6C 16r6C 16r6F 16r77 16r3A 16r20 16r49 16r4E 16r56 16r49 16r54 16r45 16r2C
                  16r20 16r41 16r43 16r4B 16r2C 16r20 16r42 16r59 16r45 16r2C 16r20 16r43 16r41 16r4E 16r43 16r45
                  16r4C 16r2C 16r20 16r4F 16r50 16r54 16r49 16r4F 16r4E 16r53 16r2C 16r20 16r4D 16r45 16r53 16r53
                  16r41 16r47 16r45 16r2C 16r20 16r55 16r50 16r44 16r41 16r54 16r45 16r2C 16r20 16r49 16r4E 16r46
                  16r4F 16r2C 16r20 16r52 16r45 16r47 16r49 16r53 16r54 16r45 16r52 16r2C 16r20 16r52 16r45 16r46
                  16r45 16r52 16r2C 16r20 16r4E 16r4F 16r54 16r49 16r46 16r59 16r2C 16r20 16r50 16r55 16r42 16r4C
                  16r49 16r53 16r48 16r2C 16r20 16r53 16r55 16r42 16r53 16r43 16r52 16r49 16r42 16r45 16r0D 16r0A
                  16r53 16r75 16r70 16r70 16r6F 16r72 16r74 16r65 16r64 16r3A 16r20 16r74 16r69 16r6D 16r65 16r72
                  16r2C 16r20 16r70 16r72 16r65 16r63 16r6F 16r6E 16r64 16r69 16r74 16r69 16r6F 16r6E 16r2C 16r20
                  16r70 16r61 16r74 16r68 16r2C 16r20 16r72 16r65 16r70 16r6C 16r61 16r63 16r65 16r73 16r0D 16r0A
                  16r41 16r6C 16r6C 16r6F 16r77 16r2D 16r45 16r76 16r65 16r6E 16r74 16r73 16r3A 16r20 16r74 16r61
                  16r6C 16r6B 16r2C 16r20 16r68 16r6F 16r6C 16r64 16r2C 16r20 16r70 16r72 16r65 16r73 16r65 16r6E
                  16r63 16r65 16r2C 16r20 16r64 16r69 16r61 16r6C 16r6F 16r67 16r2C 16r20 16r6C 16r69 16r6E 16r65
                  16r2D 16r73 16r65 16r69 16r7A 16r65 16r2C 16r20 16r63 16r61 16r6C 16r6C 16r2D 16r69 16r6E 16r66
                  16r6F 16r2C 16r20 16r73 16r6C 16r61 16r2C 16r20 16r69 16r6E 16r63 16r6C 16r75 16r64 16r65 16r2D
                  16r73 16r65 16r73 16r73 16r69 16r6F 16r6E 16r2D 16r64 16r65 16r73 16r63 16r72 16r69 16r70 16r74
                  16r69 16r6F 16r6E 16r2C 16r20 16r70 16r72 16r65 16r73 16r65 16r6E 16r63 16r65 16r2E 16r77 16r69
                  16r6E 16r66 16r6F 16r2C 16r20 16r6D 16r65 16r73 16r73 16r61 16r67 16r65 16r2D 16r73 16r75 16r6D
                  16r6D 16r61 16r72 16r79 16r2C 16r20 16r72 16r65 16r66 16r65 16r72 16r0D 16r0A 16r43 16r6F 16r6E
                  16r74 16r65 16r6E 16r74 16r2D 16r54 16r79 16r70 16r65 16r3A 16r20 16r61 16r70 16r70 16r6C 16r69
                  16r63 16r61 16r74 16r69 16r6F 16r6E 16r2F 16r73 16r64 16r70 16r0D 16r0A 16r43 16r6F 16r6E 16r74
                  16r65 16r6E 16r74 16r2D 16r44 16r69 16r73 16r70 16r6F 16r73 16r69 16r74 16r69 16r6F 16r6E 16r3A
                  16r20 16r73 16r65 16r73 16r73 16r69 16r6F 16r6E 16r0D 16r0A 16r43 16r6F 16r6E 16r74 16r65 16r6E
                  16r74 16r2D 16r4C 16r65 16r6E 16r67 16r74 16r68 16r3A 16r20 16r32 16r34 16r35 16r0D 16r0A 16r52
                  16r65 16r6D 16r6F 16r74 16r65 16r2D 16r50 16r61 16r72 16r74 16r79 16r2D 16r49 16r44 16r3A 16r20
                  16r22 16r39 16r31 16r39 16r38 16r22 16r20 16r3C 16r73 16r69 16r70 16r3A 16r39 16r31 16r39 16r38
                  16r40 16r31 16r37 16r32 16r2E 16r31 16r36 16r2E 16r31 16r2E 16r37 16r32 16r3E 16r3B 16r70 16r61
                  16r72 16r74 16r79 16r3D 16r63 16r61 16r6C 16r6C 16r69 16r6E 16r67 16r3B 16r70 16r72 16r69 16r76
                  16r61 16r63 16r79 16r3D 16r6F 16r66 16r66 16r3B 16r73 16r63 16r72 16r65 16r65 16r6E 16r3D 16r6E
                  16r6F 16r0D 16r0A 16r0D 16r0A 16r76 16r3D 16r30 16r0D 16r0A 16r6F 16r3D 16r46 16r72 16r65 16r65
                  16r53 16r57 16r49 16r54 16r43 16r48 16r20 16r31 16r33 16r30 16r37 16r36 16r32 16r35 16r30 16r30
                  16r35 16r20 16r31 16r33 16r30 16r37 16r36 16r32 16r35 16r30 16r30 16r36 16r20 16r49 16r4E 16r20
                  16r49 16r50 16r34 16r20 16r31 16r37 16r32 16r2E 16r31 16r36 16r2E 16r31 16r2E 16r37 16r32 16r0D
                  16r0A 16r73 16r3D 16r46 16r72 16r65 16r65 16r53 16r57 16r49 16r54 16r43 16r48 16r0D 16r0A 16r63
                  16r3D 16r49 16r4E 16r20 16r49 16r50 16r34 16r20 16r31 16r37 16r32 16r2E 16r31 16r36 16r2E 16r31
                  16r2E 16r37 16r32 16r0D 16r0A 16r74 16r3D 16r30 16r20 16r30 16r0D 16r0A 16r6D 16r3D 16r61 16r75
                  16r64 16r69 16r6F 16r20 16r31 16r38 16r32 16r35 16r38 16r20 16r52 16r54 16r50 16r2F 16r41 16r56
                  16r50 16r20 16r38 16r20 16r31 16r30 16r31 16r0D 16r0A 16r61 16r3D 16r72 16r74 16r70 16r6D 16r61
                  16r70 16r3A 16r38 16r20 16r50 16r43 16r4D 16r41 16r2F 16r38 16r30 16r30 16r30 16r0D 16r0A 16r61
                  16r3D 16r72 16r74 16r70 16r6D 16r61 16r70 16r3A 16r31 16r30 16r31 16r20 16r74 16r65 16r6C 16r65
                  16r70 16r68 16r6F 16r6E 16r65 16r2D 16r65 16r76 16r65 16r6E 16r74 16r2F 16r38 16r30 16r30 16r30
                  16r0D 16r0A 16r61 16r3D 16r66 16r6D 16r74 16r70 16r3A 16r31 16r30 16r31 16r20 16r30 16r2D 16r31
                  16r36 16r0D 16r0A 16r61 16r3D 16r73 16r69 16r6C 16r65 16r6E 16r63 16r65 16r53 16r75 16r70 16r70
                  16r3A 16r6F 16r66 16r66 16r20 16r2D 16r20 16r2D 16r20 16r2D 16r20 16r2D 16r0D 16r0A 16r61 16r3D
                  16r70 16r74 16r69 16r6D 16r65 16r3A 16r32 16r30 16r0D 16r0A) asByteArray asString.

            res := self parse: data.
    ]

    testByeOkay [
        | data res |

        data := #(16r53 16r49 16r50 16r2F 16r32 16r2E 16r30 16r20 16r32 16r30 16r30 16r20 16r4F 16r4B 16r0D 16r0A
                  16r56 16r69 16r61 16r3A 16r20 16r53 16r49 16r50 16r2F 16r32 16r2E 16r30 16r2F 16r55 16r44 16r50
                  16r20 16r31 16r37 16r32 16r2E 16r31 16r36 16r2E 16r32 16r35 16r34 16r2E 16r32 16r34 16r3B 16r72
                  16r70 16r6F 16r72 16r74 16r3D 16r35 16r30 16r36 16r30 16r3B 16r62 16r72 16r61 16r6E 16r63 16r68
                  16r3D 16r7A 16r39 16r68 16r47 16r34 16r62 16r4B 16r65 16r6A 16r66 16r77 16r6A 16r66 16r76 16r71
                  16r0D 16r0A 16r46 16r72 16r6F 16r6D 16r3A 16r20 16r22 16r7A 16r65 16r63 16r6B 16r65 16r22 16r20
                  16r3C 16r73 16r69 16r70 16r3A 16r31 16r30 16r30 16r30 16r40 16r6F 16r6E 16r2D 16r77 16r61 16r76
                  16r65 16r73 16r2E 16r63 16r6F 16r6D 16r3E 16r3B 16r74 16r61 16r67 16r3D 16r77 16r6D 16r79 16r63
                  16r6C 16r0D 16r0A 16r54 16r6F 16r3A 16r20 16r3C 16r73 16r69 16r70 16r3A 16r39 16r31 16r39 16r38
                  16r40 16r31 16r37 16r32 16r2E 16r31 16r36 16r2E 16r31 16r2E 16r37 16r32 16r3E 16r3B 16r74 16r61
                  16r67 16r3D 16r61 16r74 16r30 16r39 16r74 16r63 16r79 16r38 16r31 16r44 16r44 16r58 16r46 16r0D
                  16r0A 16r43 16r61 16r6C 16r6C 16r2D 16r49 16r44 16r3A 16r20 16r6F 16r66 16r63 16r77 16r6E 16r70
                  16r6D 16r75 16r6C 16r6D 16r63 16r65 16r61 16r73 16r67 16r40 16r78 16r69 16r61 16r6F 16r79 16r75
                  16r0D 16r0A 16r43 16r53 16r65 16r71 16r3A 16r20 16r39 16r38 16r34 16r20 16r42 16r59 16r45 16r0D
                  16r0A 16r55 16r73 16r65 16r72 16r2D 16r41 16r67 16r65 16r6E 16r74 16r3A 16r20 16r46 16r72 16r65
                  16r65 16r53 16r57 16r49 16r54 16r43 16r48 16r2D 16r6D 16r6F 16r64 16r5F 16r73 16r6F 16r66 16r69
                  16r61 16r2F 16r31 16r2E 16r30 16r2E 16r68 16r65 16r61 16r64 16r2D 16r67 16r69 16r74 16r2D 16r64
                  16r66 16r66 16r34 16r31 16r61 16r66 16r20 16r32 16r30 16r31 16r31 16r2D 16r30 16r34 16r2D 16r32
                  16r30 16r20 16r31 16r34 16r2D 16r31 16r31 16r2D 16r32 16r34 16r20 16r2B 16r30 16r32 16r30 16r30
                  16r0D 16r0A 16r41 16r6C 16r6C 16r6F 16r77 16r3A 16r20 16r49 16r4E 16r56 16r49 16r54 16r45 16r2C
                  16r20 16r41 16r43 16r4B 16r2C 16r20 16r42 16r59 16r45 16r2C 16r20 16r43 16r41 16r4E 16r43 16r45
                  16r4C 16r2C 16r20 16r4F 16r50 16r54 16r49 16r4F 16r4E 16r53 16r2C 16r20 16r4D 16r45 16r53 16r53
                  16r41 16r47 16r45 16r2C 16r20 16r55 16r50 16r44 16r41 16r54 16r45 16r2C 16r20 16r49 16r4E 16r46
                  16r4F 16r2C 16r20 16r52 16r45 16r47 16r49 16r53 16r54 16r45 16r52 16r2C 16r20 16r52 16r45 16r46
                  16r45 16r52 16r2C 16r20 16r4E 16r4F 16r54 16r49 16r46 16r59 16r2C 16r20 16r50 16r55 16r42 16r4C
                  16r49 16r53 16r48 16r2C 16r20 16r53 16r55 16r42 16r53 16r43 16r52 16r49 16r42 16r45 16r0D 16r0A
                  16r53 16r75 16r70 16r70 16r6F 16r72 16r74 16r65 16r64 16r3A 16r20 16r74 16r69 16r6D 16r65 16r72
                  16r2C 16r20 16r70 16r72 16r65 16r63 16r6F 16r6E 16r64 16r69 16r74 16r69 16r6F 16r6E 16r2C 16r20
                  16r70 16r61 16r74 16r68 16r2C 16r20 16r72 16r65 16r70 16r6C 16r61 16r63 16r65 16r73 16r0D 16r0A
                  16r43 16r6F 16r6E 16r74 16r65 16r6E 16r74 16r2D 16r4C 16r65 16r6E 16r67 16r74 16r68 16r3A 16r20
                  16r30 16r0D 16r0A 16r0D 16r0A) asByteArray asString.

            res := self parse: data.
    ]

    testByeRequest [
        | data res |

        data := #(16r42 16r59 16r45 16r20 16r73 16r69 16r70 16r3A 16r39 16r31 16r39 16r38 16r40 16r31 16r37 16r32
                  16r2E 16r31 16r36 16r2E 16r31 16r2E 16r37 16r32 16r3A 16r35 16r30 16r36 16r30 16r3B 16r74 16r72
                  16r61 16r6E 16r73 16r70 16r6F 16r72 16r74 16r3D 16r75 16r64 16r70 16r20 16r53 16r49 16r50 16r2F
                  16r32 16r2E 16r30 16r0D 16r0A 16r56 16r69 16r61 16r3A 16r20 16r53 16r49 16r50 16r2F 16r32 16r2E
                  16r30 16r2F 16r55 16r44 16r50 16r20 16r31 16r37 16r32 16r2E 16r31 16r36 16r2E 16r32 16r35 16r34
                  16r2E 16r32 16r34 16r3B 16r72 16r70 16r6F 16r72 16r74 16r3B 16r62 16r72 16r61 16r6E 16r63 16r68
                  16r3D 16r7A 16r39 16r68 16r47 16r34 16r62 16r4B 16r65 16r6A 16r66 16r77 16r6A 16r66 16r76 16r71
                  16r0D 16r0A 16r4D 16r61 16r78 16r2D 16r46 16r6F 16r72 16r77 16r61 16r72 16r64 16r73 16r3A 16r20
                  16r37 16r30 16r0D 16r0A 16r54 16r6F 16r3A 16r20 16r3C 16r73 16r69 16r70 16r3A 16r39 16r31 16r39
                  16r38 16r40 16r31 16r37 16r32 16r2E 16r31 16r36 16r2E 16r31 16r2E 16r37 16r32 16r3E 16r3B 16r74
                  16r61 16r67 16r3D 16r61 16r74 16r30 16r39 16r74 16r63 16r79 16r38 16r31 16r44 16r44 16r58 16r46
                  16r0D 16r0A 16r46 16r72 16r6F 16r6D 16r3A 16r20 16r22 16r7A 16r65 16r63 16r6B 16r65 16r22 16r20
                  16r3C 16r73 16r69 16r70 16r3A 16r31 16r30 16r30 16r30 16r40 16r6F 16r6E 16r2D 16r77 16r61 16r76
                  16r65 16r73 16r2E 16r63 16r6F 16r6D 16r3E 16r3B 16r74 16r61 16r67 16r3D 16r77 16r6D 16r79 16r63
                  16r6C 16r0D 16r0A 16r43 16r61 16r6C 16r6C 16r2D 16r49 16r44 16r3A 16r20 16r6F 16r66 16r63 16r77
                  16r6E 16r70 16r6D 16r75 16r6C 16r6D 16r63 16r65 16r61 16r73 16r67 16r40 16r78 16r69 16r61 16r6F
                  16r79 16r75 16r0D 16r0A 16r43 16r53 16r65 16r71 16r3A 16r20 16r39 16r38 16r34 16r20 16r42 16r59
                  16r45 16r0D 16r0A 16r55 16r73 16r65 16r72 16r2D 16r41 16r67 16r65 16r6E 16r74 16r3A 16r20 16r54
                  16r77 16r69 16r6E 16r6B 16r6C 16r65 16r2F 16r31 16r2E 16r34 16r2E 16r32 16r0D 16r0A 16r43 16r6F
                  16r6E 16r74 16r65 16r6E 16r74 16r2D 16r4C 16r65 16r6E 16r67 16r74 16r68 16r3A 16r20 16r30 16r0D
                  16r0A 16r0D 16r0A) asByteArray asString.

            res := self parse: data.
    ]

    testMultiLineResponse [
        | data crlf res |

        crlf := Character cr asString, Character nl asString.
        data := 'SIP/2.0 200 OK', crlf,
                'Via: SIP/2.0/UDP server10.biloxi.com', crlf,
                '   ;branch=z9hG4bKnashds8;received=192.0.2.3', crlf,
                'Via: SIP/2.0/UDP bigbox3.site3.atlanta.com', crlf,
                '   ;branch=z9hG4bK77ef4c2312983.1;received=192.0.2.2', crlf,
                'Via: SIP/2.0/UDP pc33.atlanta.com', crlf,
                '   ;branch=z9hG4bK776asdhds ;received=192.0.2.1', crlf,
                'To: Bob <sip:bob@biloxi.com>;tag=a6c85cf', crlf,
                'From: Alice <sip:alice@atlanta.com>;tag=1928301774', crlf,
                'Call-ID: a84b4c76e66710@pc33.atlanta.com', crlf,
                'CSeq: 314159 INVITE', crlf,
                'Contact: <sip:bob@192.0.2.4>', crlf,
                'Content-Type: application/sdp', crlf,
                'Content-Length: 0', crlf,
                crlf.

        res := self parse: data.
    ]

    testResponse [
        | data crlf res |
        crlf := Character cr asString, Character nl asString.
        data := 'SIP/2.0 480 Temporarily Unavailable', crlf,
                'Via: SIP/2.0/UDP 172.16.254.34;branch=z9hG4bKMzQ4NTQzNDgxNCwyNDE1Nw__', crlf,
                'From: <sip:1000@on-waves.com>;tag=MzQ4NTQ0MTg2NzIyNDEwNjkyNjY_', crlf,
                'To: <sip:9198@172.16.1.72>;tag=42eBv22Fj314N', crlf,
                'Call-ID: MzY3NzE3ODgyNw__@xiaoyu', crlf,
                'CSeq: 1 INVITE', crlf,
                'User-Agent: FreeSWITCH-mod_sofia/1.0.head-git-dff41af 2011-04-20 14-11-24 +0200', crlf,
                'Accept: application/sdp', crlf,
                'Allow: INVITE, ACK, BYE, CANCEL, OPTIONS, MESSAGE, UPDATE, INFO, REGISTER, REFER, NOTIFY, PUBLISH, SUBSCRIBE', crlf,
                'Supported: timer, precondition, path, replaces', crlf,
                'Allow-Events: talk, hold, presence, dialog, line-seize, call-info, sla, include-session-description, presence.winfo, message-summary, refer', crlf,
                'Reason: Q.850;cause=96;text="MANDATORY_IE_MISSING"', crlf,
                'Content-Length: 0', crlf,
                'Remote-Party-ID: "9198" <sip:9198@172.16.1.72>;party=calling;privacy=off;screen=no', crlf, crlf.

        res := self parse: data.
    ]

    testCancelResponse [
        | data res |

        data := #(83 73 80 47 50 46 48 32 52 56 49 32 67 97 108 108 47 84 114 97 110 115 97 99 116 105 111 110 32 68 111 101 115 32 78 111 116 32 69 120 105 115 116 13 10 86 105 97 58 32 83 73 80 47 50 46 48 47 85 68 80 32 49 55 50 46 49 54 46 50 53 50 46 49 52 53 58 54 54 54 54 59 98 114 97 110 99 104 61 122 57 104 71 52 98 75 77 122 81 52 78 84 85 121 77 106 99 121 78 105 119 48 79 84 77 51 77 103 95 95 13 10 70 114 111 109 58 32 60 115 105 112 58 49 48 48 48 64 115 101 99 114 101 116 108 97 98 115 46 100 101 62 59 116 97 103 61 77 122 81 52 78 84 85 121 79 84 107 121 77 84 77 50 78 122 81 120 77 122 69 121 77 122 81 95 13 10 84 111 58 32 60 115 105 112 58 57 49 57 56 64 49 55 50 46 49 54 46 49 46 55 50 62 59 116 97 103 61 121 72 78 112 75 116 78 54 50 97 116 68 97 13 10 67 97 108 108 45 73 68 58 32 77 84 77 122 78 84 77 51 78 84 81 52 78 103 95 95 64 120 105 97 111 121 117 13 10 67 83 101 113 58 32 53 48 32 67 65 78 67 69 76 13 10 67 111 110 116 101 110 116 45 76 101 110 103 116 104 58 32 48 13 10 13 10 ) asByteArray.

        res := self parse: data asString.
    ]


    testEarlyDialogResponse [
        | data res |
        "Responses from  101 to 199 on INVITE create an early dialog. Mostly
        a statement that someting is in progress.."

        data := #(83 73 80 47 50 46 48 32 49 48 49 32 68 105 97 108 111 103 32 69 115 116 97 98 108 105 115 104 101 109 101 110 116 13 10 86 105 97 58 32 83 73 80 47 50 46 48 47 85 68 80 32 48 46 48 46 48 46 48 58 53 48 54 54 59 98 114 97 110 99 104 61 122 57 104 71 52 98 75 77 122 85 121 77 84 89 119 78 84 73 120 78 83 119 120 78 84 99 49 59 114 101 99 101 105 118 101 100 61 49 50 55 46 48 46 48 46 49 13 10 70 114 111 109 58 32 60 115 105 112 58 49 48 48 48 64 115 105 112 46 122 101 99 107 101 46 111 115 109 111 99 111 109 46 111 114 103 62 59 116 97 103 61 77 122 85 121 77 84 89 120 77 106 81 120 78 84 77 52 78 84 103 121 78 68 65 53 78 84 73 95 13 10 84 111 58 32 60 115 105 112 58 49 50 51 52 53 54 64 49 50 55 46 48 46 48 46 49 62 59 116 97 103 61 51 56 54 53 51 48 57 51 52 13 10 67 97 108 108 45 73 68 58 32 77 84 103 121 77 106 107 48 77 68 69 49 79 81 95 95 64 115 97 110 109 105 110 103 122 101 13 10 67 83 101 113 58 32 49 32 73 78 86 73 84 69 13 10 67 111 110 116 97 99 116 58 32 60 115 105 112 58 49 50 51 52 53 54 64 49 57 50 46 49 54 56 46 50 46 50 51 57 58 53 48 54 48 62 13 10 85 115 101 114 45 65 103 101 110 116 58 32 76 105 110 112 104 111 110 101 47 51 46 52 46 51 32 40 101 88 111 115 105 112 50 47 51 46 54 46 48 41 13 10 67 111 110 116 101 110 116 45 76 101 110 103 116 104 58 32 48 13 10 13 10 ) asByteArray.
        res := self parse: data asString.
    ]

    testQuotedString [
        | res |
        "Just test that it doesn't loop forever"
        res := SIPGrammar new quoted_string parse: '"'.
        self assert: res isPetitFailure.
    ]

    testWithAuthorizationNeeded [
        | data res |
        data := (WriteStream on: String new)
            nextPutAll: 'SIP/2.0 401 Unauthorized'; cr; nl;
            nextPutAll: 'Via: SIP/2.0/UDP 172.16.252.198:5060;branch=z9hG4bK6cdba079-8b91-e311-8101-844bf52a8297;rport=5060;received=172.16.252.198'; cr; nl;
            nextPutAll: 'From: <sip:st@127.0.0.2>;tag=12187969-8b91-e311-8101-844bf52a8297'; cr; nl;
            nextPutAll: 'To: <sip:st@127.0.0.2>'; cr; nl;
            nextPutAll: 'Call-ID: fc0f7969-8b91-e311-8101-844bf52a8297@xiaoyu'; cr; nl;
            nextPutAll: 'CSeq: 7 REGISTER'; cr; nl;
            nextPutAll: 'WWW-Authenticate: Digest realm="Yate", nonce="373ef30b297545cbce99fad09f1409cb.1392124197", stale=TRUE, algorithm=MD5'; cr; nl;
            nextPutAll: 'Proxy-Authenticate: Digest realm="07440491",qop="auth",nonce="7a7155d2bff57ffcc226f0e6819d00be68d517b3C0A4ABEB5BE0",algorithm=MD5'; cr; nl;
            nextPutAll: 'Server: YATE/5.1.0'; cr; nl;
            nextPutAll: 'Allow: ACK, INVITE, BYE, CANCEL, REGISTER, REFER, OPTIONS, INFO'; cr; nl;
            nextPutAll: 'Content-Length: 0'; cr; nl;
            cr; nl;
            contents.

        res := self parse: data asString.
    ]

    testAuthorization [
        | res |
        res := SIPGrammar new Authorization parse: 'Authorization: Digest username="st", realm="Yate", nonce="373ef30b297545cbce99fad09f1409cb.1392124197", uri="sip:127.0.0.1", algorithm=MD5, response="bc8dfaa413e897863dbab4c622e4b9b4"'.
        self assert: res first equals: 'Authorization'.
        self assert: res third size equals: 3.
    ]
]
