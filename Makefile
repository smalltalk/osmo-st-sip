
GST_PACKAGE = gst-package
GST_CONVERT = gst-convert

CONVERT_RULES = -r'Osmo.LogManager->LogManager' \
		-r'Osmo.LogArea->LogArea'  \
		-r'Osmo.LogLevel->LogLevel' \
                -r'Osmo.TimerScheduler->TimerScheduler' \
		-r'Osmo.OsmoUDPSocket->OsmoUDPSocket' \
                -r'Sockets.StreamSocket->SocketStream' \
		-r'Osmo.SIPParser->SIPParser' \
		-r'DateTime->DateAndTime' \
		-r'Character nl->Character lf' \
		-r'(Duration milliseconds: ``@args1) -> (Duration milliSeconds: ``@args1)' \
		-r'PP.PPParser->PPParser' \
		-r'PP.PPCompositeParser->PPCompositeParser' \
		-r'PP.PPCompositeParserTest->PPCompositeParserTest' \
		-r'PP.PPPredicateObjectParser->PPPredicateObjectParser' \
		-r'PP.PPCharSetPredicate->PPCharSetPredicate' \
		-r'Osmo.MessageBuffer->MessageBuffer' \
		-r'SystemExceptions.NotFound->NotFound' \
		-r'(``@object substrings: ``@args1)->(``@object subStrings: ``@args1)' \
		-r'(Dictionary from: ``@args1)->(Dictionary newFrom: ``@args1)' \
		-r'(``@object copyFrom: ``@args1)->(``@object copyFrom: ``@args1 to: ``@object size)' \
		-r'Sockets.SocketAddress->GSTSocketAddress' \
		-r'((Osmo at: \#SIPParser) combineUri: ``@args1)->(SIPParser combineUri: ``@args1)' \
		-r'``@object nl->``@object lf' \
		-r'RecursionLock->Mutex' \
		-r'SystemExceptions.EndOfStream->Exception' \
		-r'Sockets.Datagram->OsmoUDPDatagram' \
		-r'Sockets.DatagramSocket new->Socket newUDP' \
		-r'(``@obj startsWith: ``@arg2 )->(``@obj beginsWith: ``@arg2)'  \

# Can not be parsed right now..
#		-r'(``@object => ``@args1)->(``@object ==> ``@args1)'

GRAMMAR = \
	grammar/SIPQuotedStringParser.st \
	grammar/SIPGrammar.st grammar/SIPGrammarTest.st \
	grammar/SIPQuotedStringParserTest.st


CALLAGENT = \
	callagent/Base64MimeConverter.st \
	callagent/SIPLogArea.st \
	callagent/SIPDialog.st \
	callagent/misc/SIPBase64.st \
	callagent/misc/SIPRandomHelper.st \
	callagent/misc/SIPURandom.st \
	callagent/parameters/Extensions.st \
	callagent/parameters/SIPParam.st \
	callagent/parameters/SIPGenericParam.st \
	callagent/parameters/SIPCSeq.st \
	callagent/parameters/SIPToFromParam.st \
	callagent/parameters/SIPVia.st \
	callagent/requests/SIPRequest.st \
	callagent/requests/SIPACKRequest.st \
	callagent/requests/SIPByeRequest.st \
	callagent/requests/SIPCancelRequest.st \
	callagent/requests/SIPInviteRequest.st \
	callagent/requests/SIPOptionsRequest.st \
	callagent/requests/SIPRegisterRequest.st \
	callagent/parser/Extensions.st \
	callagent/parser/SIPParser.st \
	callagent/SIPResponse.st \
	callagent/SIPIdentity.st \
	callagent/transactions/SIPTransaction.st \
	callagent/transactions/SIPByeTransaction.st \
	callagent/transactions/SIPInviteTransaction.st \
	callagent/transactions/SIPRegisterTransaction.st \
	callagent/useragent/Extensions.st \
	callagent/useragent/SIPUserAgentBase.st \
	callagent/useragent/SIPUserAgent.st \
	callagent/session/Extensions.st \
	callagent/session/SIPSessionBase.st \
	callagent/session/SIPCallBase.st \
	callagent/session/SIPCall.st \
	callagent/session/SIPIncomingCall.st \
	callagent/transport/SIPTransport.st \
	callagent/transport/SIPUdpTransport.st \
	callagent/authorization/SIPDigest.st \
	callagent/authorization/SIPAuthorization.st \
	callagent/authorization/SIPProxyAuthorization.st \
	callagent/tests/SIPParserTest.st \
	callagent/tests/Tests.st \
	callagent/tests/SIPCallAgentTest.st \
	callagent/tests/SIPDigestTest.st \
	callagent/tests/SIPTransportMock.st \
	callagent/tests/SIPRegisterTransactionTest.st \
	callagent/tests/SIPInviteTest.st \
	callagent/tests/SIPBase64Test.st

PHARO_COMPAT = pharo-porting/compat_for_pharo.st
PHARO_CHANGES = pharo-porting/changes_for_pharo.st


all:
	$(GST_PACKAGE) --test package.xml

convert:
	$(GST_CONVERT) $(CONVERT_RULES) -F squeak -f gst \
		-C-MIMERWStream \
		-C-MimeConverter \
		-o fileout.st $(PHARO_COMPAT) \
		$(GRAMMAR) $(CALLAGENT) \
		$(PHARO_CHANGES)
	sed -i s,"=>","==>",g fileout.st
