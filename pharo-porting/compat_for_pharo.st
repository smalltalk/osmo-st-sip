Object subclass: GSTSocketAddress [
    <category: 'OsmoSIP-Pharo'>
    <comment: 'TODO: Move it into the OsmoNetwork package'>

    GSTSocketAddress class >> localHostName [
	^ NetNameResolver localHostName
    ]

    GSTSocketAddress class >> loopbackHost [
        ^'127.0.0.1'
    ]

    GSTSocketAddress class >> byName: aName [
	^NetNameResolver addressForName: aName
    ]
]
