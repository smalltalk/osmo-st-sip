"
 (C) 2011,2014 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

TestCase subclass: SIPRegisterTransactionTest [
    | sent transport agent dialog |
    <category: 'OsmoSIP-Callagent-Tests'>

    createSimple401: aBranch callId: aCallId tag: aTag cseq: aCseq [
        ^(WriteStream on: String new)
            nextPutAll: 'SIP/2.0 401 Unauthorized'; cr; nl;
            nextPutAll: 'Via: SIP/2.0/UDP 127.0.0.1:5060;branch='; nextPutAll: aBranch; cr; nl;
            nextPutAll: 'From: <sip:st@127.0.0.1>;tag='; nextPutAll: aTag; cr; nl;
            nextPutAll: 'To: <sip:st@127.0.0.1>'; cr; nl;
            nextPutAll: 'Call-ID: '; nextPutAll: aCallId; cr; nl;
            nextPutAll: 'CSeq: '; nextPutAll: aCseq asString; nextPutAll: ' REGISTER'; cr; nl;
            nextPutAll: 'WWW-Authenticate: Digest realm="Yate", nonce="2ea525666844d310cd6d23cd6869b8fd.1393336640", stale=FALSE, algorithm=MD5'; cr; nl;
            nextPutAll: 'Server: YATE/5.1.0'; cr; nl;
            nextPutAll: 'Allow: ACK, INVITE, BYE, CANCEL, REGISTER, REFER, OPTIONS, INFO'; cr; nl;
            nextPutAll: 'Content-Length: 0'; cr; nl;
            cr; nl;
            contents
    ]

    createSimple200: aBranch callId: aCallId tag: aTag cseq: aCseq [
        ^(WriteStream on: String new)
            nextPutAll: 'SIP/2.0 200 OK'; cr; nl;
            nextPutAll: 'Via: SIP/2.0/UDP 127.0.0.1:5060;branch='; nextPutAll: aBranch; cr; nl;
            nextPutAll: 'From: <sip:st@127.0.0.1>;tag='; nextPutAll: aTag; cr; nl;
            nextPutAll: 'To: <sip:st@127.0.0.1>'; cr; nl;
            nextPutAll: 'Call-ID: '; nextPutAll: aCallId; cr; nl;
            nextPutAll: 'CSeq: '; nextPutAll: aCseq asString; nextPutAll: ' REGISTER'; cr; nl;
            nextPutAll: 'WWW-Authenticate: Digest realm="Yate", nonce="2ea525666844d310cd6d23cd6869b8fd.1393336640", stale=FALSE, algorithm=MD5'; cr; nl;
            nextPutAll: 'Server: YATE/5.1.0'; cr; nl;
            nextPutAll: 'Allow: ACK, INVITE, BYE, CANCEL, REGISTER, REFER, OPTIONS, INFO'; cr; nl;
            nextPutAll: 'Content-Length: 0'; cr; nl;
            cr; nl;
            contents
    ]

    setUp [
        | localSent |
        sent := OrderedCollection new.
        localSent := sent.
        transport := SIPTransportMock new
                        onData: [:datagram | localSent add: datagram];
                        yourself.
        agent := SIPUserAgent createOn: transport.
        agent
            username: 'st';
            password: 'st'.

        dialog := SIPDialog fromUser: 'sip:st@127.0.0.1' host: '127.0.0.1' port: 5060.
        dialog identity: agent mainIdentity.
    ]

    testSimpleRegister [
        | register branch callId fromTag msg |

        register := (SIPRegisterTransaction createWith: dialog on: agent cseq: 1)
                        destination: 'sip:127.0.0.1';
                        yourself.
        register start.
        self assert: sent size equals: 1.
        msg := agent parser parse: sent first data.
        self assert: (msg parameter: 'CSeq' ifAbsent: [-1]) number equals: 1.

        "Now inject an auth requirement message"
        branch := (msg parameter: 'Via' ifAbsent: [nil]) branch.
        callId := (msg parameter: 'Call-ID' ifAbsent: [-1]).
        fromTag := (msg parameter: 'From' ifAbsent: [nil]).
        transport inject: (self createSimple401: branch callId: callId tag: fromTag tag cseq: 1).
        self assert: sent size equals: 3.
        msg := agent parser parse: sent second data.
        self assert: msg class verb equals: 'ACK'.

        msg := agent parser parse: sent third data.
        self assert: msg class verb equals: 'REGISTER'.
        self assert: (msg parameter: 'CSeq' ifAbsent: [-1]) number equals: 2.
        branch := (msg parameter: 'Via' ifAbsent: [nil]) branch.
        transport inject: (self createSimple200: branch callId: callId tag: fromTag tag cseq: 2).

        self assert: register state equals: SIPTransaction stateCompleted.

        "Cancel the transaction"
        register
            onTimeout: [];
            timedout.
    ]
]
