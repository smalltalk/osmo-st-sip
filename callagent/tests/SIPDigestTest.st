TestCase subclass: SIPDigestTest [
    <category: 'OsmoSIP-Callagent-Tests'>
    <comment: 'I test the digest implemetnation'>

    testSimpleDigest [
        | res |
        res := SIPDigest authUser: 'st' password: 'st' realm: 'Yate' nonce: '373ef30b297545cbce99fad09f1409cb.1392124197' operation: 'REGISTER' url: 'sip:127.0.0.2'.
        self assert: res equals: '987937460705015b6575176176d7c739'.
    ]

    testDigestQop [
        | res |
        res := SIPDigest authUser: 'stUser' password: 'stPassword' realm: 'Bla' nonce: '373ef30b297545cbce99fad09f1409cb.1392124197' operation: 'INVITE' url: 'sip:127.0.0.1' qop: 'auth' clientNonce: '01234567' nonceCount: '00000001'.
        self assert: res equals: 'f7514463e9589f372d8021498e5d6ce3'.
    ]
]
