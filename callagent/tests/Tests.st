"
 (C) 2011-2012 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

TestCase subclass: SIPRequestTest [
    <category: 'OsmoSIP-Callagent-Tests'>

    testINVITE [
        | dialog req out |

        dialog := SIPDialog fromUser: 'sip:1000@on-foo.com' host: '192.168.0.106' port: 5060.
        dialog fromTag: 'MzQ4NjQ4MTg0Mjc0MzAzODU5NA__'.
        dialog to: 'sip:9198@192.168.0.106'.

        req := SIPInviteRequest from: dialog.

        out := (WriteStream on: String new)
                nextPutAll: 'INVITE sip:9198@192.168.0.106 SIP/2.0'; cr; nl;
                nextPutAll: 'To: <sip:9198@192.168.0.106>'; cr; nl;
                nextPutAll: 'From: <sip:1000@on-foo.com>;tag=MzQ4NjQ4MTg0Mjc0MzAzODU5NA__'; cr; nl;
                nextPutAll: 'Contact: <sip:1000@on-foo.com>'; cr; nl;
                cr; nl; contents.

        self assert: req asDatagram equals: out.
    ]

    testOPTIONS [
        | dialog req out |

        dialog := SIPDialog fromUser: 'sip:1000@on-foo.com' host: '192.168.0.106' port: 5060.
        dialog fromTag: 'MzQ4NjQ4MTg0Mjc0MzAzODU5NA__'.
        dialog to: 'sip:9198@192.168.0.106'.

        req := SIPOptionsRequest from: dialog.

        out := (WriteStream on: String new)
                nextPutAll: 'OPTIONS sip:9198@192.168.0.106 SIP/2.0'; cr; nl;
                nextPutAll: 'To: <sip:9198@192.168.0.106>'; cr; nl;
                nextPutAll: 'From: <sip:1000@on-foo.com>;tag=MzQ4NjQ4MTg0Mjc0MzAzODU5NA__'; cr; nl;
                nextPutAll: 'Contact: <sip:1000@on-foo.com>'; cr; nl;
                nextPutAll: 'Accept: application/sdp'; cr; nl;
                cr; nl; contents.
        self assertSame: req asDatagram and: out.
    ]

    assertSame: got and: want [
        <category: 'tests'>

        got = want
            ifTrue:  [self assert: true]
            ifFalse: [
              Transcript
                  nextPutAll: 'Got: "';
                  nextPutAll: got displayString;
                  nextPut: $"; nl.
              self assert: false].
    ]
]

TestCase subclass: SIPUdpTransportTest [
    <category: 'OsmoSIP-Callagent-Tests'>

    testSending [
        | target transp datagram read |

        target := Sockets.DatagramSocket new.
        datagram := Sockets.Datagram new.
        datagram port: target port.
        datagram address: target address.
        datagram data: 'foooo'.

        transp := SIPUdpTransport startOn: '127.0.0.1'.
        transp start.
        transp queueDatagram: datagram.

        (Delay forSeconds: 5)
            value: [read := target next]
            onTimeoutDo: [].

        self deny: read data isNil.
        self assert: read size equals: 5.
        self assert: (read data copyFrom: 1 to: 5) equals: 'foooo' asByteArray.
    ]
]
