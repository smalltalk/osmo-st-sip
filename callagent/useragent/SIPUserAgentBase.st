"
 (C) 2011-2014 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

Object subclass: SIPUserAgentBase [
    | transport name |
    <category: 'OsmoSIP-Callagent'>
    <comment: 'I am a user agent base'>

    SIPUserAgentBase class >> createOn: aTransport [
        <category: 'factory'>
        ^ self new
            transport: aTransport;
            yourself
    ]

    SIPUserAgentBase class >> branchStart [
        <category: 'ids'>
        "magic marker..."
        ^ 'z9hG4bK'
    ]

    SIPUserAgentBase class >> generateBranch [
        | data |
        data := '<1p>,<2p>' expandMacrosWithArguments:
                    {DateTime now asUTC asSeconds. Random between: 0 and: 99999}.
        ^ self branchStart, (SIPBase64 encode: data).
    ]

    SIPUserAgentBase class >> generateCSeq [
        <category: 'helper'>
        "CSeq must be < 2^31 but Random only allows 2^29"
        ^ Random between: 1 and: (2 raisedTo: 29).
    ]


    injectDefaults: aRequest [
        aRequest addParameter: 'Max-Forwards' value: '70'.
        aRequest addParameter: 'User-Agent' value: self name.
    ]

    generateVia: aBranch [
        <category: 'ids'>
        ^ (WriteStream on: String new)
                nextPutAll: 'SIP/2.0/';
                nextPutAll: transport type;
                nextPutAll: ' ';
                nextPutAll: transport address printString;
                nextPutAll: ':';
                nextPutAll: transport port asString;
                nextPutAll: ';branch=';
                nextPutAll: aBranch;
                nextPutAll: ';rport';
                contents.
    ]

    name [
        <category: 'accessing'>
        ^ name ifNil: ['OsmoST-SIP 0.34']
    ]

    transport: aTransport [
        transport ifNotNil: [transport handler: nil].

        transport := aTransport.
        transport handler: self.
    ]

    transport [
        <category: 'accessing'>
        ^ transport
    ]

    queueData: aData dialog: aDialog [
        | datagram |
        <category: 'output'>

        datagram := Sockets.Datagram
                                data: aData
                                address: (Sockets.SocketAddress
                                            byName: aDialog destIp)
                                port: aDialog destPort.
        transport queueDatagram: datagram.
    ]
]
