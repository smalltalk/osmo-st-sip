"
 (C) 2011 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

Object subclass: SIPDigest [
    <category: 'OsmoSIP-Callagent-Auth'>
    <comment: 'I help in generating a response to a nonce/digest
    request.'>

    SIPDigest class >> ha1: aName realm: aRealm password: aPassword [
        ^(MD5 new
            nextPutAll: aName;
            nextPutAll: ':';
            nextPutAll: aRealm;
            nextPutAll: ':';
            nextPutAll: aPassword;
            digest) hex.
    ]

    SIPDigest class >> ha2: anOperation uri: aSipUrlString [
        ^(MD5 new
            nextPutAll: anOperation;
            nextPutAll: ':';
            nextPutAll: aSipUrlString;
            digest) hex.
    ]

    SIPDigest class >> authUser: aName password: aPassword realm: aRealm nonce: aNonce operation: anOperation url: aSipUrlString [
        | ha1 ha2 resp md5 |
        ha1 := self ha1: aName realm: aRealm password: aPassword.
        ha2 := self ha2: anOperation uri: aSipUrlString.

        ^(MD5 new
            nextPutAll: ha1;
            nextPutAll: ':';
            nextPutAll: aNonce;
            nextPutAll: ':';
            nextPutAll: ha2;
            digest) hex
    ]

    SIPDigest class >> authUser: aName password: aPassword realm: aRealm nonce: aNonce operation: anOperation url: aSipUrlString qop: aQop clientNonce: aCnonce nonceCount: aNc [
        | ha1 ha2 resp md5 |
        ha1 := self ha1: aName realm: aRealm password: aPassword.
        ha2 := self ha2: anOperation uri: aSipUrlString.

        ^(MD5 new
            nextPutAll: ha1;
            nextPutAll: ':';
            nextPutAll: aNonce;
            nextPutAll: ':';
            nextPutAll: aNc;
            nextPutAll: ':';
            nextPutAll: aCnonce;
            nextPutAll: ':';
            nextPutAll: aQop;
            nextPutAll: ':';
            nextPutAll: ha2;
            digest) hex
    ]
]
