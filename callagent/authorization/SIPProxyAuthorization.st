"
 (C) 2011,2014 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

SIPAuthorization subclass: SIPProxyAuthorization [
    | qop clientNonce nonceCount |
    <category: 'OsmoSIP-authorization'>
    <comment: 'I help with the proxy authorization'>

    SIPProxyAuthorization class >> validCalls [
        ^super validCalls, #(#qop: #cnonce: #nc:)
    ]

    SIPProxyAuthorization class >> formatNonceCount: aCount [
        ^aCount printPaddedWith: $0 to: 8.
    ]

    SIPProxyAuthorization class >> new [
        ^self basicNew initialize
    ]

    initialize [
        nonceCount := 0.
    ]

    qop [
        <category: 'accessing'>
        ^qop
    ]

    qop: aQop [
        <category: 'accessing'>
        qop := aQop
    ]

    cnonce: aCNonce [
        aCNonce size = 8 ifFalse: [^self error: 'clientNonce needs to be eight chars'].
        clientNonce := aCNonce
    ]

    cnonce [
        ^clientNonce
    ]

    clientNonce [
        ^clientNonce
    ]

    nonceCount: aCount [
        nonceCount := aCount
    ]

    nc: aCount [
        ^self nonceCount: aCount
    ]

    nonceCount [
        ^nonceCount
    ]

    incrementClientNonce [
        nonceCount := nonceCount + 1.
    ]

    calculateResponse: aPassword operation: anOperationName [
        response := SIPDigest
                        authUser: username
                        password: aPassword
                        realm: realm
                        nonce: nonce
                        operation: anOperationName
                        url: uri
                        qop: qop
                        clientNonce: clientNonce
                        nonceCount: (self class formatNonceCount: nonceCount).
    ]

    nextPutAllOn: aStream [
        aStream
            nextPutAll: 'Digest username="';
            nextPutAll: username;
            nextPutAll: '", realm="';
            nextPutAll: realm;
            nextPutAll: '", nonce="';
            nextPutAll: nonce;
            nextPutAll: '", uri="';
            nextPutAll: uri;
            nextPutAll: '", algorithm=MD5, response="';
            nextPutAll: response;
            nextPutAll: '", cnonce=';
            nextPutAll: clientNonce;
            nextPutAll: ', qop=';
            nextPutAll: qop;
            nextPutAll: ', nc=';
            nextPutAll: (self class formatNonceCount: nonceCount).
    ]
]
