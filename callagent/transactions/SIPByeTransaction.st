"
 (C) 2011,2014 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

SIPTransaction subclass: SIPByeTransaction [
    <category: 'OsmoSIP-Callagent'>

    SIPByeTransaction class >> operationName [
        ^SIPByeRequest verb
    ]

    transmit [
        | bye |
        bye := self createBye: initial_dialog.
        self queueData: bye asDatagram dialog: initial_dialog.
    ]
]
