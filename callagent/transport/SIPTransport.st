"
 (C) 2011-2012 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

Object subclass: SIPTransport [
    | handler |
    <category: 'OsmoSIP-Callagent'>
    <comment: 'I am the baseclass for a transport'>

    SIPTransport class >> type [
        <category: 'accessing'>
        ^ self subclassResponsibility
    ]

    handleData: aData [
        <category: 'private'>
        [
            handler isNil
                ifTrue:  [self logError: 'No handler for data.' area: #sip]
                ifFalse: [handler transportData: self data: aData]
        ] on: Error do: [:e |
            self logException: ('HandleData <1p>'
                                    expandMacrosWith: e tag) area: #sip
        ]
    ]

    handler: aHandler [
        <category: 'configuration'>
        handler := aHandler
    ]

    address [
        <category: 'accessing'>
        ^ self subclassResponsibility
    ]

    port [
        <category: 'accessing'>
        ^ self subclassResponsibility
    ]

    type [
        <category: 'accessing'>
        ^ self class type
    ]
]

