"
 (C) 2011-2012 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

SIPTransport subclass: SIPUdpTransport [
    | socket net |
    <category: 'OsmoSIP-Callagent'>
    <comment: 'I should share some things with MGCPCallAgent'>

    SIPUdpTransport class >> type [
        <category: 'accessing'>
        ^ 'UDP'
    ]

    SIPUdpTransport class >> startOn: anAddress port: aPort [
        <category: 'factory'>

        ^ (self new)
            initialize: anAddress port: aPort;
            yourself
    ]

    SIPUdpTransport class >> startOn: anAddress [
        ^ self startOn: anAddress port: 5060.
    ]

    address [
        <category: 'accessing'>
        ^ socket address
    ]

    port [
        <category: 'accessing'>
        ^ socket port
    ]

    initialize: anAddress port: aPort [
        socket := Sockets.DatagramSocket local: anAddress port: aPort.
        socket
            bufferSize: 2048;
            addToBeFinalized.

        net := Osmo.OsmoUDPSocket new
          name: 'SIPTransport';
          onData: [:data |self handleData: data];
          yourself
    ]

    queueDatagram: aDatagram [
        <category: 'output'>
        net queueData: aDatagram.
    ]

    stop [
        <category: 'lifetime'>
        net stop.
        socket := nil.
    ]

    start [
        net start: socket.
    ]
]
