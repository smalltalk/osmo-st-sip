"
 (C) 2011-2014 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

SIPGrammar subclass: SIPParser [
    <category: 'OsmoSIP-Parser'>
    <comment: 'I create either a SIPRequest or a SIPResponse'>

    SIPParser class >> addArrayRec: anArray on: aStream [
        anArray isNil ifTrue: [^false].

        anArray do: [:each |
            each isArray
                ifTrue:  [self addArrayRec: each on: aStream]
                ifFalse: [
                    each isString ifTrue: [aStream nextPutAll: each].
                    each isCharacter ifTrue: [aStream nextPut: each].
                    each isGenericSIPParam ifTrue: [aStream nextPutAll: each asFoldedString]
                ].
        ]
    ]

    SIPParser class >> combineUri: anArray [
        | str |
        str := WriteStream on: (String new: 20).
        self addArrayRec: anArray on: str.
        ^ str contents
    ]

    Request [
        ^ super Request => [:nodes | SIPRequest parseFrom: nodes ]
    ]

    Response [
        ^ super Response => [:nodes | SIPResponse parseFrom: nodes ]
    ]

    SIPURI [
        ^ super SIPURI => [:nodes | self class combineUri: nodes]
    ]

    SIPSURI [
        ^ super SIPSURI => [:nodes | self class combineUri: nodes]
    ]

    URI [
        ^super URI flatten
    ]

    via_ttl [
        <category: 'via'>
        ^ super via_ttl => [:nodes | SIPGenericParam fromMandatory: nodes]
    ]

    via_maddr [
        <category: 'via'>
        ^ super via_maddr => [:nodes | SIPGenericParam fromMandatory: nodes]
    ]

    via_received [
        <category: 'via'>
        ^ super via_received => [:nodes | SIPGenericParam fromMandatory: nodes]
    ]

    via_branch [
        <category: 'via'>
        ^ super via_branch => [:nodes | SIPGenericParam fromMandatory: nodes]
    ]

    response_port [
        <category: 'via'>
        ^ super response_port => [:nodes | SIPGenericParam fromOptional: nodes]
    ]

    generic_param [
        ^ super generic_param => [:nodes |
            SIPGenericParam fromOptional: nodes].
    ]

    tag_param [
        ^ super tag_param => [:nodes |
            SIPGenericParam fromMandatory: nodes].
    ]

    From [
        ^ super From => [:nodes |
            Array
                with: nodes first
                with: nodes second
                with: (SIPToFromParam parseFrom: nodes third)]
    ]

    To [
        ^ super To => [:nodes |
            Array
                with: nodes first
                with: nodes second
                with: (SIPToFromParam parseFrom: nodes third)]
    ]

    Via [
        ^ super Via => [:nodes |
            Array
                with: nodes first
                with: nodes second
                with: (SIPVia parseFrom: nodes third)
                with: nodes fourth
        ]
    ]

    CSeq [
        ^ super CSeq => [:nodes |
            Array
                with: nodes first
                with: nodes second
                with: (SIPCSeq parseFrom: nodes third)]
    ]

    IPv4address [
        ^ super IPv4address flatten
    ]

    EQUAL [
        ^super EQUAL flatten
    ]

    stale [
        ^super stale => [:nodes |
            Array
                with: nodes first
                with: nodes second
                with: nodes third asLowercase = 'true']
    ]

    qop_options [
        "TODO: There can be multiple auth options to pick from"
        ^super qop_options => [:nodes |
            Array
                with: nodes first
                with: nodes second
                with: nodes fourth].
    ]

    challenge [
        ^super challenge => [:nodes |
            | d |
            d := Dictionary new.
            (nodes at: 2) do: [:each |
                "Skip the separator for now until we have withoutSeparators"
                each isCharacter ifFalse: [
                    d at: each first put: each third]].
            d]
    ]

    request_digest [
        ^super request_digest => [:nodes | nodes second]
    ]

    digest_uri [
        ^super digest_uri => [:nodes |
                Array
                    with: nodes first
                    with: nodes second
                    with: nodes fourth]
    ]

    username [
        ^super username => [:nodes |
                Array
                    with: nodes first
                    with: nodes second
                    with: nodes third]
    ]

    Authorization [
        ^super Authorization => [:nodes |
                | params |
                params := OrderedCollection new.
                params add: nodes third third first.
                nodes third third second do: [:each |
                    params add: each second].
                Array
                    with: nodes first
                    with: nodes second
                    with: (SIPAuthorization from: params)]
    ]

    ProxyAuthorization [
        ^super ProxyAuthorization => [:nodes |
                | params |
                params := OrderedCollection new.
                params add: nodes third third first.
                nodes third third second do: [:each |
                    params add: each second].
                Array
                    with: nodes first
                    with: nodes second
                    with: (SIPProxyAuthorization from: params)]
    ]
]
