"
 (C) 2014 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

Object subclass: SIPIdentity [
    | hostname username password proxyUsername proxyPassword |

    <category: 'OsmoSIP-authorization'>
    <comment: 'I represent a local identity that initiated a session
    or similar'>

    username: aUsername [
        username := aUsername
    ]

    password: aPassword [
        password := aPassword
    ]

    hostname: aHostname [
        hostname := aHostname
    ]

    proxyUsername: aUsername [
        proxyUsername := aUsername
    ]

    proxyPassword: aPassword [
        proxyPassword := aPassword
    ]

    username [
        ^username
    ]

    password [
        ^password
    ]

    hostname [
        ^hostname
    ]

    proxyUsername [
        ^proxyUsername ifNil: [username]
    ]

    proxyPassword [
        ^proxyPassword ifNil: [password]
    ]
]
